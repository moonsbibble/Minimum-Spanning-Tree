%%%% -*- Mode: Prolog -*-

:- dynamic graph/1.
:- dynamic vertex/2.
:- dynamic arc/4.
:- dynamic heap_entry/4.
:- dynamic heap/2.
:- dynamic vertex_key/3.
:- dynamic vertex_previous/3.
:- dynamic node/4.
% :- use_module(library(csv)).

%%%------------------------------GRAPH STRUCTURE------------------------------

%%% new_graph/1
new_graph(G) :- graph(G), !.
new_graph(G) :- assert(graph(G)).

%%% new_vertex/2
new_vertex(G, V) :-
    new_graph(G),
    vertex(G, V), !.
new_vertex(G, V) :-
    new_graph(G),
    assert(vertex(G, V)).

%%% new_arc/4
new_arc(G, U, V, Weight) :-
    (arc(G, U, V, Weight);
     U == V), !.
new_arc(G, U, V, Weight) :-
    retract(arc(G, V, U, _)),
    assert(arc(G, U, V, Weight)), !.
new_arc(G, U, V, Weight) :-
    retract(arc(G, U, V, _)),
    assert(arc(G, U, V, Weight)), !.
new_arc(G, U, V, Weight) :-
    new_graph(G),
    new_vertex(G, V),
    new_vertex(G, U),
    assert(arc(G, U, V, Weight)).

%%% new_arc/3
new_arc(G, U, V) :- new_arc(G, U, V, 1).

%%% delete_graph/1
delete_graph(G) :-
    retractall(graph(G)),
    retractall(vertex(G, _)),
    retractall(arc(G, _, _, _)),
    retractall(vertex_key(G, _, _)),
    retractall(vertex_previous(G, _, _)).

%%% graph_vertices/2
graph_vertices(G, Nw) :-
    findall(vertex(G, V), vertex(G, V), Vs),
    permutation(Vs, Nw).

%%% graph_arcs/2
graph_arcs(G, Nw) :-
    findall(arc(G, V, U, W), arc(G, V, U, W), Es),
    permutation(Es, Nw).

%%% vertex_neighbours/3
vertex_neighbours(G, V, Nw) :-
    findall(arc(G, V, U, W), (arc(G, U, V, W); arc(G, V, U, W)), As),
    permutation(As, Nw).

%%% fast_neighbours/3
fast_neighbours(G, V, As) :-
    findall(arc(G, V, U, W), (arc(G, U, V, W); arc(G, V, U, W)), As).

%%% adjs/3
adjs(G, V, Nw) :-
    findall(vertex(G, N), (arc(G, V, N, _); arc(G, N, V, _)), Vs),
    permutation(Vs, Nw).

%%% list_graph/1
list_vertices(G) :-
  graph(G),
  listing(vertex(G, _)).

%%% list_arcs/1
list_arcs(G) :-
  graph(G),
  listing(arc(G, _, _, _)).

%%% list_vertices/1
list_graph(G) :-
    graph(G),
    list_arcs(G),
    list_vertices(G).

%%% read_graph/2
read_graph(G, FileName) :-
    delete_graph(G),
    new_graph(G),
    csv_read_file(FileName,
		  Data,
		  [functor(table),
		   arity(3),
		   separator(0'\t)]),
    save_graph(G, Data).

%%% save_graph/2
save_graph(G, [D | Ds]) :-
    arg(1, D, V),
    arg(2, D, U),
    arg(3, D, Weight),
    new_vertex(G, V),
    new_vertex(G, U),
    new_arc(G, V, U, Weight),
    save_graph(G, Ds), !.
save_graph(_, []).

%%% write_graph/2
write_graph(G, FileName) :-
    graph(G),
    write_graph(G, FileName, graph).

%%% write_graph/3
write_graph(G, FileName, Type) :-
    Type == 'graph',
    graph(G),
    findall(arc(U, V, Weight),
	    arc(G, U, V, Weight),
	    Data),
    csv_write_file(FileName,
		   Data,
		   [functor(table),
		    arity(3),
		    separator(0'\t)]), !.
write_graph(G, FileName, Type) :-
    Type == 'edge',
    arc_edit(G, Data),
    csv_write_file(FileName,
		   Data,
		   [functor(table),
		    arity(3),
		    separator(0'\t)]).

%%% arc_edit/2
arc_edit([], []).
arc_edit([arc(_, U, V, Weight) | As], [arc(U, V, Weight) | Es]) :-
    arc_edit(As, Es).

%%%------------------------------HEAP STRUCTURE------------------------------

%%% new_heap/1
new_heap(H) :- heap(H, _), !.
new_heap(H) :- assert(heap(H, 0)).

%%% delete_heap/1
delete_heap(H) :-
    retractall(heap(H, _)),
    retractall(heap_entry(H, _, _, _)).

%%% heap_size/2
heap_has_size(H, S) :- heap(H, S).

%%% incr_heap/1
incr_heap(H) :-
    retract(heap(H, S)),
    S1 is S + 1,
    assert(heap(H, S1)).

%%% decr_heap/1
decr_heap(H) :-
    retract(heap(H, S)),
    S1 is S - 1,
    assert(heap(H, S1)).

%%% heap_empty/1
heap_empty(H) :- heap(H, 0).

%%% heap_not_empty/1
heap_not_empty(H) :-
    heap(H, _),
    not(heap_empty(H)).

%%% heap_head/3
heap_head(H, K, V) :-
    heap(H, _),
    heap_entry(H, 1, K, V).

%%% modify_key/4
modify_key(H, NewKey, OldKey, V) :-
    retract(heap_entry(H, P, OldKey, V)),
    assert(heap_entry(H, P, NewKey, V)),
    I is truncate(P/2),
    heapify_reverse(H, I),
    heapify(H, P).

%%% heap_exchange/3
heap_exchange(H, P, P) :- heap(H, _), !.
heap_exchange(H, P1, P2) :-
    heap(H, _),
    retract(heap_entry(H, P1, K1, V1)),
    retract(heap_entry(H, P2, K2, V2)),
    assert(heap_entry(H, P2, K1, V1)),
    assert(heap_entry(H, P1, K2, V2)).

%%% list_heap/1
list_heap(H) :-
    listing(heap(H, _)),
    listing(heap_entry(H, _, _, _)).

%%% heapify_reverse/2
heapify_reverse(H, I) :-
    heap(H, _),
    heap_fix(H, I, I, r).

%%% heapify/2
heapify(H, I) :-
    heap(H, _),
    heap_fix(H, I, I, f).

%%% heap_fix/4
heap_fix(_, 0, _, r) :- !.
heap_fix(H, I, Min, Flag) :-
    Left is 2 * I,
    Right is Left + 1,
    (Min \= Left,
     P = Left;
     Min \= Right,
     P = Right),
    heap_entry(H, P, K1, _),
    heap_entry(H, Min, K2, _),
    K1 @< K2,
    heap_fix(H, I, P, Flag), !.
heap_fix(H, I, Min, r) :-
    Min \= I,
    heap_exchange(H, I, Min),
    I @> 1,
    P is truncate(I / 2),
    heap_fix(H, P, P, r), !.
heap_fix(H, I, Min, f) :-
    Min \= I,
    heap_exchange(H, I, Min),
    heap_fix(H, Min, Min, f), !.
heap_fix(_, I, I, _) :- !.
heap_fix(_, 1, _, r).

%%% heap_insert/3
heap_insert(H, K, V) :-
    not(heap_entry(H, _, _, V)),
    heap_has_size(H, Size),
    P is Size + 1,
    assert(heap_entry(H, P, K, V)),
    incr_heap(H),
    I is truncate(P/2),
    heapify_reverse(H, I).

%%% heap_extract/3
heap_extract(H, K, V) :-
    heap_head(H, K, V),
    heap_has_size(H, Size),
    heap_exchange(H, 1, Size),
    retract(heap_entry(H, Size, _, _)),
    decr_heap(H),
    heapify(H, 1).

%%%------------------------------MST STRUCTURE------------------------------

%%% delete_mst/1
delete_mst(G) :-
    retractall(vertex_key(G, _, _)),
    retractall(vertex_previous(G, _, _)).

%%% mst_prim/2
mst_prim(G, Source) :-
    delete_mst(G),
    vertex(G, Source),
    graph_vertices(G, Vs),
    new_node(G, Vs),
    delete_heap(h),
    new_heap(h),
    heap_insert(h, 0, Source),
    prim_execution(G).

%%% prim_execution/1
prim_execution(G) :-
    heap_extract(h, K, U),
    retract(vertex_key(G, U, _)),
    assert(vertex_key(G, U, K)),
    fast_neighbours(G, U, Arcs),
    update_key(G, U, Arcs),
    prim_execution(G), !.
prim_execution(_) :-
  heap_has_size(h, 0),
  delete_heap(h).

%%% new_node/2
new_node(G, [X | Xs]) :-
    arg(2, X, V),
    assert(vertex_previous(G, V, -1)),
    assert(vertex_key(G, V, inf)),
    new_node(G, Xs), !.
new_node(_, []).

%%% update_key/3
update_key(G, U, [arc(G, U, V, W) | Arcs]) :-
    heap_entry(h, _, HK, V),
    (W @>= HK;
     modify_key(h, W, HK, V),
     retract(vertex_previous(G, V, _)),
     assert(vertex_previous(G, V, U))),
    update_key(G, U, Arcs), !.
update_key(G, U, [arc(G, U, V, W) | Arcs]) :-
    vertex_key(G, V, VK),
    (VK \= inf;
     heap_insert(h, W, V),
     retract(vertex_previous(G, V, _)),
     assert(vertex_previous(G, V, U))),
    update_key(G, U, Arcs), !.
update_key(_, _, []).

%%% list_mst/1
list_mst(G) :-
    listing(vertex_key(G, _, _)),
    listing(vertex_previous(G, _, _)).

%%% mst_get/3
mst_get(G, Source, PreorderTree) :-
    retractall(node(_, _, _, _)),
    findall(node(G, V, U, K),
	    (vertex_previous(G, V, U), U \= -1,
	     vertex_key(G, V, K)), Ps),
    init_get(Ps),
    preorder(G, Source, [], PreorderTree).

%%% preorder/4
preorder(G, Source, Gp, [arc(G, Source, P, K) | Arcs]) :-
    findall(node(V, K), node(G, V, Source, K), Ns),
    length(Ns, Len),
    Len @> 1,
    min_arc(Ns, K),
    remove_eccept(Ns, K, Nw),
    sort(Nw, [P | _]),
    retract(node(G, P, Source, K)),
    preorder(G, P, [Source | Gp], Arcs), !.
preorder(G, Source, Gp, [arc(G, Source, P, K) | Arcs]) :-
    retract(node(G, P, Source, K)),
    preorder(G, P, Gp, Arcs), !.
preorder(G, _, [Parent | Gp], Arcs) :-
    preorder(G, Parent, Gp, Arcs), !.
preorder(G, _, _, []) :- not(node(G, _, _, _)).

%%% init_get/1
init_get([A | Arcs]) :-
    assert(A),
    init_get(Arcs).
init_get([]).

%%% remove_eccept/3
remove_eccept([node(V, K) | Ns], K, [V | Nw]) :-
    remove_eccept(Ns, K, Nw), !.
remove_eccept([node(_, K1) | Ns], K, Nw) :-
    K1 \= K,
    remove_eccept(Ns, K, Nw), !.
remove_eccept([], _, []).

%%% min_arc/2
min_arc([node(_, K1) | Ns], Min) :-
    minimum(Ns, K1, Min).

%%% minimum/3
minimum([node(_, K1) | Ns], Min0, Min) :-
    (K1 @< Min0,
     Min1 = K1;
     Min1 = Min0),
    minimum(Ns, Min1, Min).
minimum([], Min, Min).
