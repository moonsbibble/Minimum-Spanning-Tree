(defparameter *vertices*
  (make-hash-table :test #'equal))

(defparameter *graphs*
  (make-hash-table :test #'equal))

(defparameter *arcs*
  (make-hash-table :test #'equal))

(defparameter *visited*
  (make-hash-table :test #'equal))

(defparameter *vertex-keys*
  (make-hash-table :test #'equal))

(defparameter *previous*
  (make-hash-table :test #'equal))

(defparameter *heaps*
  (make-hash-table :test #'equal))

(defparameter *index*
  (make-hash-table :test #'equal))

(defparameter *neighbours*
  (make-hash-table :test #'equal))

(defparameter *nodes*
  (make-hash-table :test #'equal))

(defparameter *adjacent*
  (make-hash-table :test #'equal))

;------------------------------GRAPH STRUCTURE------------------------------

(defun is-graph (graph-id)
  (if (gethash graph-id *graphs*)
      graph-id
    nil))

(defun new-graph (graph-id)
  (or (gethash graph-id *graphs*)
      (setf (gethash graph-id *graphs*) graph-id)))

(defun delete-graph (graph-id)
  (remhash graph-id *graphs*)
  (maphash #'(lambda (k v)
	       (when (equal (first k) graph-id)
		 (remhash k *neighbours*)))
           *neighbours*)
  (maphash #'(lambda (k v)
	       (when (equal (second k) graph-id)
		 (remhash k *vertices*)))
           *vertices*)
  (maphash #'(lambda (k v)
	       (when (equal (second k) graph-id)
		 (remhash k *arcs*)))
           *arcs*)
  (maphash #'(lambda (k v)
         (when (equal (first k) graph-id)
     (remhash k *vertex-keys*)))
           *vertex-keys*)
  (maphash #'(lambda (k v)
         (when (equal (first k) graph-id)
     (remhash k *previous*)))
           *previous*))

(defun new-vertex (graph-id vertex)
  (or (gethash (list 'vertex graph-id vertex) *vertices*)
      (setf (gethash (list 'vertex graph-id vertex) *vertices*)
            (list 'vertex graph-id vertex))))

(defun graph-vertices (graph-id)
  (if (gethash graph-id *graphs*)
      (let ((l '()))
        (maphash #'(lambda (k v)
		     (when (equal (second k) graph-id)
		       (push v l)))
                 *vertices*)
        l)
    nil))

(defun new-arc (graph-id vertex1 vertex2 &optional (w 1))
  (new-graph graph-id)
  (new-vertex graph-id vertex1)
  (new-vertex graph-id vertex2)
  (if (not (gethash (list graph-id vertex1) *neighbours*))
      (setf (gethash (list graph-id vertex1) *neighbours*)
            (list (list 'arc graph-id vertex1 vertex2 w)))
    (push (list 'arc graph-id vertex1 vertex2 w)
          (gethash (list graph-id vertex1) *neighbours*)))
  (if (not (gethash (list graph-id vertex2) *neighbours*))
      (setf (gethash (list graph-id vertex2) *neighbours*)
            (list (list 'arc graph-id vertex2 vertex1 w)))
    (push (list 'arc graph-id vertex2 vertex1 w)
          (gethash (list graph-id vertex2) *neighbours*)))
  (if (gethash (list 'arc graph-id vertex2 vertex1) *arcs*)
      (remhash (list 'arc graph-id vertex2 vertex1) *arcs*))
  (or (equal vertex1 vertex2)
      (setf (gethash (list 'arc graph-id vertex1 vertex2) *arcs*)
            (list 'arc graph-id vertex1 vertex2 w))))

(defun graph-arcs (graph-id)
  (if (gethash graph-id *graphs*)
      (let ((l '()))
        (maphash #'(lambda (k v)
		     (when (equal (second k) graph-id)
		       (push v l)))
                 *arcs*)
	l)
    nil))

(defun graph-vertex-neighbours (graph-id vertex)
  (nth-value 0 (gethash (list graph-id vertex) *neighbours*)))

(defun graph-vertex-adjacent (graph-id vertex)
  (if (and (atom vertex)
           (gethash graph-id *graphs*)
           (gethash (list 'vertex graph-id vertex) *vertices*))
      (let ((l '()))
        (maphash #'(lambda (k v)
		     (cond
		      ((and (equal (second k) graph-id)
			    (equal (third k) vertex))
		       (push (list 'vertex graph-id (fourth k)) l))
		      ((and (equal (second k) graph-id)
			    (equal (fourth k) vertex))
		       (push (list 'vertex graph-id (third k)) l))))
                 *arcs*)
        l)
    nil))

(defun graph-print (graph-id)
  (if (gethash graph-id *graphs*)
      (let ((l '()))
        (maphash #'(lambda (k v)
		     (when (equal (second k) graph-id)
		       (push v l)))
                 *arcs*)
        (maphash #'(lambda (k v)
		     (when (equal (second k) graph-id)
		       (push v l)))
                 *vertices*)
        l)
    nil))

;------------------------------HEAP STRUCTURE------------------------------

(defstruct node
  key
  value
  )

(defun heap-id (heap-rep)
  (second heap-rep))

(defun heap-size (heap-rep)
  (third heap-rep))

(defun heap-actual-heap (heap-rep)
  (fourth heap-rep))

(defun new-heap (heap-id &optional (capacity 42))
  (or (gethash heap-id *heaps*)
      (setf (gethash heap-id *heaps*)
            (list 'heap heap-id 0
                  (make-array capacity
                              :fill-pointer 0
                              :adjustable t
                              :element-type '(or null node))))))

(defun heap-delete (heap-id)
  (if (remhash heap-id *heaps*)
    (not (maphash #'(lambda (k v)
                    	       (when (equal (first k) heap-id)
                           		 (remhash k *index*)))
                  *index*))
    t))

(defun heap-empty (heap-id)
  (if (gethash heap-id *heaps*)
      (cond
       ((equal (heap-size (gethash heap-id *heaps*)) 0) t)
       (t nil))
    nil))

(defun heap-not-empty (heap-id)
  (if (gethash heap-id *heaps*)
      (cond
       ((equal (heap-size (gethash heap-id *heaps*)) 0) nil)
       (t t))
    nil))

(defun heap-head (heap-id)
  (if (and (gethash heap-id *heaps*)
           (> (length (heap-actual-heap (gethash heap-id *heaps*))) 0))
      (list (node-key (aref (heap-actual-heap (gethash heap-id *heaps*)) 0))
	    (node-value (aref (heap-actual-heap (gethash heap-id *heaps*)) 0)))
    nil))

(defun heap-exchange (heap-id p0 p1 array)
  (if (/= p0 p1)
      (let ((n1 (aref array p0))
            (n2 (aref array p1)))
        (setf (aref array p0) n2)
        (setf (aref array p1) n1)
        (setf (gethash (list heap-id (node-value n1)) *index*) p1)
        (setf (gethash (list heap-id (node-value n2)) *index*) p0) n1 n2)
    t))

(defun heap-insert (heap-id k v)
  (if (and (gethash heap-id *heaps*)
           (not (gethash (list heap-id v) *index*)))
      (let ((get (gethash heap-id *heaps*))
            (p (truncate (/ (float (- (heap-size (gethash heap-id *heaps*)) 1)) 2)))
            (return t))
        (vector-push-extend (make-node :key k :value v) (heap-actual-heap get) 1)
        (setf (gethash (list heap-id v) *index*) (heap-size get))
        (setf (third get) (+ 1 (heap-size get)))
        (heapify-reverse heap-id p p (heap-actual-heap get)) get p return)
    nil))

(defun heap-extract (heap-id)
  (if (and (gethash heap-id *heaps*)
           (> (length (heap-actual-heap (gethash heap-id *heaps*))) 0))
      (and (let ((get (gethash heap-id *heaps*)))
             (heap-exchange heap-id 0 (- (heap-size get) 1) (heap-actual-heap get))
             (setf (third get) (- (heap-size get) 1)) get)
           (node-print heap-id (vector-pop (heap-actual-heap (gethash heap-id *heaps*)))
		       (gethash heap-id *heaps*)))
    nil))

(defun node-print (heap-id node get)
  (heapify heap-id 0 0 (heap-actual-heap get))
  (setf (gethash (list heap-id (node-value node)) *index*) nil)
  (list (node-key node) (node-value node)))

(defun heap-modify-key (heap-id new-key old-key v)
  (if (and (gethash heap-id *heaps*)
           (gethash (list heap-id v) *index*)
           (equal (node-key (aref (heap-actual-heap (gethash heap-id *heaps*))
                                  (gethash (list heap-id v) *index*)))
                  old-key))
      (let ((array (heap-actual-heap (gethash heap-id *heaps*)))
            (index (gethash (list heap-id v) *index*))
            (return t))
        (let ((p (truncate (/ (float (- index 1)) 2))))
          (setf (node-key (aref array index)) new-key)
          (heapify heap-id index index array)
          (heapify-reverse heap-id p p array) p)
        array index return)
    nil))

(defun modify-key (heap-id new-key v)
  (if (and (gethash heap-id *heaps*)
           (gethash (list heap-id v) *index*))
      (let ((array (heap-actual-heap (gethash heap-id *heaps*)))
            (index (gethash (list heap-id v) *index*))
            (return t))
        (let ((p (truncate (/ (float (- index 1)) 2))))
          (setf (node-key (aref array index)) new-key)
          (heapify heap-id index index array)
          (heapify-reverse heap-id p p array) p)
        array index return)
    nil))

(defun heap-print (heap-id)
  (heap-actual-heap (gethash heap-id *heaps*)))

(defun key (nodei nodem)
  (< (node-key nodei)
     (node-key nodem)))

(defun heapify (heap-id i min array)
  (let ((left (+ 1 (* 2 i)))
        (right (+ 2 (* 2 i))))
    (cond
     ((and (/= min left)
           (< left (length array))
           (key (aref array left) (aref array min)))
      (heapify heap-id i left array))
     ((and (/= min right)
           (< right (length array))
           (key (aref array right) (aref array min)))
      (heapify heap-id i right array))
     ((/= min i)
      (and (heap-exchange heap-id i min array)
           (heapify heap-id min min array)))
     ((= i min) t))
    left right))

(defun heapify-reverse (heap-id i min array)
  (let ((left (+ 1 (* 2 i)))
        (right (+ 2 (* 2 i)))
        (p (truncate (/ (float (- i 1)) 2))))
    (cond
     ((and (/= min left)
           (< left (length array))
           (key (aref array left) (aref array min)))
      (heapify-reverse heap-id i left array))
     ((and (/= min right)
           (< right (length array))
           (key (aref array right) (aref array min)))
      (heapify-reverse heap-id i right array))
     ((/= min i)
      (and (heap-exchange heap-id i min array)
           (heapify-reverse heap-id p p array)))
     ((= i min) t))
    left right p))

;------------------------------MST STRUCTURE------------------------------

(defun mst-vertex-key (graph-id vertex-id)
  (let
      ((g (gethash (list graph-id vertex-id) *vertex-keys*))) g))

(defun mst-previous (graph-id vertex-id)
  (let
      ((g (gethash (list graph-id vertex-id) *previous*))) g))

(defun mst-prim (graph-id source)
  (create-node graph-id)
  (heap-delete 'h)
  (new-heap 'h (hash-table-count *vertices*))
  (heap-insert 'h 0 source)
  (prim-execution graph-id (heap-extract 'h))
  (not (heap-delete 'h)))

(defun prim-execution (graph-id node)
  (if (not (null node))
    (progn (setf (gethash (list graph-id (second node)) *vertex-keys*)
                 (first node))
           (update-key graph-id
                       (second node)
                       (graph-vertex-neighbours graph-id (second node)))
           (prim-execution graph-id (heap-extract 'h)))
    t))

(defun create-node (graph-id)
  (maphash #'(lambda (k v)
               (when (equal (second k) graph-id)
		 (and (setf (gethash (list graph-id (third k)) *vertex-keys*)
			    MOST-POSITIVE-DOUBLE-FLOAT)
		      (setf (gethash (list graph-id (third k)) *previous*)
			    -1))))
           *vertices*))

(defun update-key (graph-id source neighbours)
  (if (> (length neighbours) 0)
      (and (let ((arc (car neighbours)))
             (let ((index (gethash (list 'h (fourth arc)) *index*))
                   (w (fifth arc))
                   (v (fourth arc)))
               (cond
                ((equal index nil)
                 (if (equal MOST-POSITIVE-DOUBLE-FLOAT
                            (gethash (list graph-id v) *vertex-keys*))
                     (and (heap-insert 'h w v)
                          (setf (gethash (list graph-id v) *previous*)
                                source))
                   t))
                (t (if (< w (node-key (aref (heap-actual-heap (gethash 'h *heaps*))
                                            index)))
                       (and (modify-key 'h w v)
                            (setf (gethash (list graph-id v) *previous*)
                                  source))
                     t)))
               index w v)
             arc)
           (update-key graph-id source (cdr neighbours)))
    t))

(defun mst-print (graph-id)
  (maphash #'(lambda (k v)
	       (when (equal (first k) graph-id)
		 (print (list 'vertex-key (second k) v))))
           *vertex-keys*)
  (maphash #'(lambda (k v)
	       (when (equal (first k) graph-id)
		 (print (list 'previous (second k) v))))
           *previous*))

(defun mst-get (graph-id source)
  (clrhash *adjacent*)
  (clrhash *nodes*)
  (not (init-get graph-id))
  (preorder graph-id source '()))

(defun preorder (graph-id source parents)
  (cond
   ((> (length (gethash (list graph-id source) *adjacent*)) 1)
    (preorder-print 0
                    graph-id
                    source
                    (sorting
                     (remove-eccept
                       (gethash (list graph-id source) *adjacent*)
                       (min-k (gethash (list graph-id source) *adjacent*))))
                    nil
                    parents
                    (gethash (list graph-id source) *adjacent*)))
   ((equal (length (gethash (list graph-id source) *adjacent*)) 1)
    (preorder-print 1
                    graph-id
                    source
                    (first (car (gethash (list graph-id source) *adjacent*)))
                    (second (car (gethash (list graph-id source) *adjacent*)))
                    parents
                    (gethash (list graph-id source) *adjacent*)))
   ((and (equal (gethash (list graph-id source) *adjacent*) nil)
         (> (hash-table-count *nodes*) 0))
    (preorder graph-id (car parents) (cdr parents)))
   (t nil)))

(defun preorder-print (id graph-id source v k parents adjacents)
  (if (= 0 id)
      (progn  (remhash (list graph-id v) *nodes*)
              (setf (gethash (list graph-id source) *adjacent*)
                    (remove-fast adjacents v))
              (cons (list 'arc
                          graph-id
                          source
                          v
                          (gethash (list graph-id v) *vertex-keys*))
                    (preorder graph-id v (push source parents))))
    (progn  (remhash (list graph-id v) *nodes*)
            (remhash (list graph-id source) *adjacent*)
            (cons (list 'arc graph-id source v k)
                  (preorder graph-id v parents)))))

(defun init-get (graph-id)
  (maphash #'(lambda (k u)
	       (when (and (equal (first k) graph-id)
			  (not (equal u -1)))
		 (and (setf (gethash k *nodes*)
                            (list u (gethash k *vertex-keys*)))
		      (if (not (gethash (list graph-id u) *adjacent*))
			  (setf
                           (gethash (list graph-id u) *adjacent*)
                           (list (list (second k) (gethash k *vertex-keys*))))
                        (push (list (second k) (gethash k *vertex-keys*))
                              (gethash (list graph-id u) *adjacent*))))))
           *previous*))

(defun get-min (m l)
  (cond
   ((null (car l)) m)
   ((< (second (car l)) m)
    (get-min (second (car l)) (cdr l)))
   (t (get-min m (cdr l)))))

(defun min-k (adjacents)
  (get-min (second (car adjacents))
           (cdr adjacents)))

(defun remove-eccept (adjacents k)
  (cond
   ((null (car adjacents)) nil)
   ((equal (second (car adjacents)) k)
    (cons (first (car adjacents))
          (remove-eccept (cdr adjacents) k)))
   (t (remove-eccept (cdr adjacents) k))))

(defun remove-fast (adjacents k)
  (cond
   ((equal (first (car adjacents)) k)
    (cdr adjacents))
   (t (cons (car adjacents)
            (remove-fast (cdr adjacents) k)))))

(defun sorting (lista)
  (alpha-first (car lista) (cdr lista)))

(defun alpha-first (m lista)
  (cond
    ((null lista) m)
    ((minor (car lista) m)
        (alpha-first (car lista) (cdr lista)))
    (t (alpha-first m (cdr lista)))))

(defun minor (a b)
  (cond
   ((and (numberp a)
	 (numberp b))
    (< a b))
   ((and (not (numberp a))
	 (numberp b))
    nil)
   ((and (numberp a)
	 (not (numberp b)))
    t)
   (t (not (null (string< (write-to-string a)
			  (write-to-string b)))))))
