# Minimum-Spanning-Tree

This is a library that offers the possibility of using the optimized MST Prim's algorithm. This project include a library in two different language: 
* Common Lisp
* Prolog

On both this files you will be able to create `graphs`, arcs and vertices, then you can use the mst-prim function to run the Prim's algorithm and generate the `MST` through different views. In addition there are severals functions to handle `binary heaps` including: insert, extract, modify a node that are also used in the mst-prim function.

## Algorithms efficiency

These are the processor cycles and times of mst-prim & mst-get functions in `Common Lisp` testing with 500K arcs and 10K vertices

<img width="664" alt="Schermata 2021-01-17 alle 14 30 40" src="https://user-images.githubusercontent.com/55984585/104845294-e5fff500-58d4-11eb-953d-d66437b35ac1.png">

<img width="575" alt="get 0 015" src="https://user-images.githubusercontent.com/55984585/104853256-e6fa4c00-58ff-11eb-998a-f24803a453e5.png">

## Graphs

Create a new graph g
```prolog
Lisp
(new-graph 'g)

Prolog
new_graph(g).
```
Delete a graph g 
```prolog
Lisp
(delete-graph 'g)

Prolog
delete_graph(g).
```
Check if g is a graph
```prolog
Lisp
(is-graph 'g)
```
Adding a vertex v in a graph g 
```prolog
Lisp
(new-vertex 'g 'v)

Prolog
new_vertex(g, v).
```
Adding an arc in a graph g, v and s are vertices and w is the weight of the arc
```prolog
Lisp
(new-arc 'g 'v 's w)

Prolog
new_arc(g, v, s, w).
```
Get all the vertices in a graph g (Xs is the returning list)
```prolog
Lisp
(graph-vertices 'g)

Prolog
graph_vertices(g, Xs).
```
Get all the Arcs in a graph g (Xs is the returning list)
```prolog
Lisp
(graph-arcs 'g)

Prolog
graph_arcs(g, Xs).
```
View a graph g 
```prolog
Lisp
(graph-print 'g)

Prolog
list_graph(g).
list_arcs(g).
list_vertices(g).
```
Get all the arc adjacent to 'v in a graph 'g (Xs is the returning list)
```prolog
Lisp
(graph-vertex-neighbours 'g 'v)

Prolog
vertex_neighbours(g, Xs).
```
Get all the vertices adjacent to 'v in a graph 'g (Xs is the returning list)
```prolog
Lisp
(graph-vertex-adjacent 'g 'v)

Prolog
adjs(g, v, Xs).
```
Read from csv file to a graph g
```prolog
Prolog
read_graph(g, Path).
```
Write to csv file (with constant edge you need to add a list of Arcs in As)
```prolog
Prolog
write_graph(g, Path).
write_graph(As, Path, edge).
```

##  Heap

Create a new heap h with size s (Prolog version always starts from size 0)
```prolog
Lisp
(new-heap 'h s)

Prolog
new_heap(h).
```
Length of the heap h (L is the returning value)
```prolog
Prolog
heap_has_size(h, L)
```
Check if the heap h is empty or not
```prolog
Lisp
(heap-empty 'h)
(heap-not-empty 'h)

Prolog
heap_empty(h).
heap_not_empty(h).
```
Insert in heap h with key k (number) and value v
```prolog
Lisp
(heap-insert 'h k 'v)

Prolog
heap_insert(h, k, v).
```
Extract from heap h, the returning values are: key k (number) and value v
```prolog
Lisp
(heap-extract 'h)

Prolog
heap_extract(h, K, V).
```
View the first node withou extract it
```prolog
Lisp
(heap-head 'h)

Prolog
heap_head(h, K, V).
```
Modify a key in heap h with value 'v and k as the new key
```prolog
Lisp
(modify-key 'h k 'v)

Prolog
modify_key(h, k, v).
```
View the heap h
```prolog
Lisp
(heap-print 'h)

Prolog
list_heap(h).
```

## MST

Call the Prim's algorithm (g is the graph and v is the source node)
```prolog
Lisp
(mst-prim 'g 'v)

Prolog
mst_prim(g, v).
```
View the MST generated from the graph g
```prolog
Lisp
(mst-print 'g)

Prolog
list_mst(g).
```
Print the MST generated in preorder, choosing the minor weight and with the same weight choosing the alphabetically previous
```prolog
Lisp
(mst-get 'g 'v)

Prolog
mst_get(g, v).
```
Get the sum produced from all the weight of the arcs that appear calling mst-get
```prolog
Lisp
(get-sum 'g 'v)
```
